import { Fragment } from 'react';
import { useLocation } from 'react-router-dom';
import InnerPageHeader from '../components/Layout/InnerPageHeader';
import ImageItem from '../components/UI/ImageItem';
import DetailPage from './DetailPage';
import img from '../assets/images/img-thumb.jpeg';
import imgHero from '../assets/images/detail-img-hero.jpeg';

const DUMMY_IMAGES = [
    {
        id: 1,
        imgSrc: img,
        imgHeroSrc: imgHero,
        title: 'Image title 1',
        category: ['Nordic', 'Earth'],
        colour: ['green', 'yellow', 'brown']
    },
    {
        id: 12,
        imgSrc: img,
        imgHeroSrc: imgHero,
        title: 'Image title 2',
        category: ['Nordic', 'Earth'],
        colour:  ['green', 'yellow', 'brown']
    },
    {
        id: 3,
        imgSrc: img,
        imgHeroSrc: imgHero,
        title: 'Image title 3',
        category: ['Nordic', 'Earth'],
        colour:  ['green', 'yellow', 'brown']
    },
]


const GetInspired = () => {
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);
    const queryIndex =  queryParams.get('index');

    let queryScreen;

    if (location.search !== ''){
        const contentIndex = DUMMY_IMAGES.findIndex((cur, i, arr) => arr[i].id === +queryIndex);
        const { imgHeroSrc, title: imgHeroTitle } = DUMMY_IMAGES[contentIndex];
        queryScreen =  <DetailPage imgHero={imgHeroSrc} imgHeroAlt={imgHeroTitle} />
    }else{
        queryScreen = DUMMY_IMAGES.map(img => (<ImageItem key={img.id} linkTo={`${location.pathname}?index=${img.id}`} img={ {src: img.imgSrc, alt: img.title, id: img.id} }/>))
    }

    return (
        <Fragment>
            { !queryIndex && <InnerPageHeader title="Get Inspired" backLinkTo="/" text="Back to frontpage" />}
            { queryScreen }
        </Fragment>
    );
}

export default GetInspired;