import { Fragment } from 'react';
import { useLocation } from 'react-router-dom';
import classes from './DetailPage.module.scss';
import BackButton from '../components/UI/BackButton';


const DetailPage = (props) => {
    const location = useLocation();
    const query = new URLSearchParams(location.search).get('index');
    return (
        <Fragment>
            <div className={classes['detail']}>
                <p>Detail page for page query {query}</p>
                <BackButton linkTo={location.pathname} className={classes['detail__link-back']} />
                <div className="detail-slider">
                    <div className="detail-slider__image"><img src={props.imgHero} alt={props.imgHeroAlt} /></div>
                </div>
            </div>
        </Fragment>
    )
}

export default DetailPage;