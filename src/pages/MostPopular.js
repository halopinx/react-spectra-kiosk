import InnerPageHeader from '../components/Layout/InnerPageHeader';
const MostPopular = () => {
    return <InnerPageHeader title="Most Popular" backLinkTo="/" />;
}

export default MostPopular;