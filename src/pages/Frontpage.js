import { Fragment } from 'react';
import Box from '../components/UI/Box';
import ContentFlex from '../components/UI/Wrappers/ContentFlex';
import imgbox1 from '../assets/images/get-inspired.jpeg';
import imgbox2 from '../assets/images/most-popular.jpeg';
import imgbox3 from '../assets/images/latest-offer.jpeg';

const Frontpage = () => {
    return (
        <Fragment>
            <ContentFlex>
                <Box title="Get Inspired" image={imgbox1} linkTo="/get-inspired" />
                <Box title="Most Popular" image={imgbox2} linkTo="/most-popular"/>
                <Box title="Latest Offers" image={imgbox3} linkTo="/latest-offers" />
            </ContentFlex>
        </Fragment>
    )
}

export default Frontpage;