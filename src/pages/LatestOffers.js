import InnerPageHeader from '../components/Layout/InnerPageHeader';

const LatestOffers = () => {
    return <InnerPageHeader title="Latest Offers" backLinkTo="/" />;
}

export default LatestOffers;