import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Frontpage from './pages/Frontpage';
import GetInspired from './pages/GetInspired';
import MostPopular from './pages/MostPopular';
import LatestOffers from './pages/LatestOffers';
import MainNavigation from './components/Layout/MainNavigation';
function App() {
  return (
      <Router>
          <MainNavigation />

          <Switch>
              <Route path='/' exact>
                  <Frontpage />
              </Route>
              <Route path='/get-inspired' exact>
                 <GetInspired/>
              </Route>
              <Route path='/most-popular'>
                  <MostPopular/>
              </Route>
              <Route path='/latest-offers'>
                  <LatestOffers/>
              </Route>
              <Route path='*'>
                 <p>Page not found</p>
              </Route>
          </Switch>
      </Router>
  );
}

export default App;
