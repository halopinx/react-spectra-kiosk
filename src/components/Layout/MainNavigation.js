import { Fragment, useState } from 'react';
import { Link } from 'react-router-dom';
import MenuButton from '../../components/UI/MenuButton';
import classes from './MainNavigation.module.scss';


const MainNavigation = () => {
    const [showMenu, setShowMenu] = useState(false);
    const toggleMenuHandler = () => {
        setShowMenu(!showMenu);
    }
    const closeMenuHandler = () => {
        setShowMenu(false);
    }
    return (
        <Fragment>
            <div className={`${classes['main-nav']} ${showMenu ? classes['is-shown'] : ''}` }>
                <nav className={classes['nav-menu']}>
                    <ul className={classes['nav-menu__list']}>
                        <li><Link to='/get-inspired' onClick={closeMenuHandler}>Get Inspired</Link></li>
                        <li><Link to='/most-popular' onClick={closeMenuHandler}>Most Popular</Link></li>
                        <li><Link to='/latest-offers' onClick={closeMenuHandler}>Latest Offers</Link></li>
                    </ul>
                </nav>
                <button onClick={closeMenuHandler} className={`button-menu--close ${classes.close}`}>Close</button>
            </div>
            <MenuButton onToggleMenu={toggleMenuHandler} />
        </Fragment>
    )
}

export default MainNavigation;