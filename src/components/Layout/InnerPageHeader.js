import classes from './InnerPageHeader.module.scss';
import BackButton from '../../components/UI/BackButton';
import Select from '../../components/UI/Select';

const InnerPageHeader = (props) => {
    const colourItems = ['Green', 'Yellow', 'Orange'];
    const categoryItems = ['Nordic', 'Metallic', 'Earth'];
    return (
        <header className={classes.header}>
            <BackButton linkTo={props.backLinkTo} text={props.text} className={classes['header__link']}/>
            <h1 className={classes['header__title']}>{props.title}</h1>
            <Select select={{ name: 'byColour', id: 'byColour'}} selectItems={colourItems}/>
            <Select select={{ name: 'byCategory', id: 'byCategory'}} selectItems={categoryItems} />
        </header>
    )
}

export default InnerPageHeader;