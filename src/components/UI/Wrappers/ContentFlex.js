import classes from './ContentFlex.module.scss';
const ContentFlex = props => {
    return (
        <div className={classes['content-flex']}>{props.children}</div>
    )
}

export default ContentFlex;