import { Link } from 'react-router-dom';
import classes from './BackButton.module.scss'

const BackButton = (props) => {
    return (
        <Link to={props.linkTo} className={`${props.className} ${classes['link-back']}`}>{ !props.text ? 'Back to index' : props.text}</Link>
    )
}

export default BackButton;