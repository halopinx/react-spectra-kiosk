const MenuButton = (props) => {
    return <button className='button-menu' onClick={props.onToggleMenu}>Menu</button>
}

export default MenuButton;