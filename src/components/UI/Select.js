const Select = (props) => {
    const items = props.selectItems.map((item, index) => (<option key={index} value={item.toLocaleLowerCase()}>{item}</option>));
    return <select {...props.select}>
        {items}
    </select>
}

export default Select;