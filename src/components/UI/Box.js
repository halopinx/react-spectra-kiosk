import { Link } from 'react-router-dom';
import classes from './Box.module.scss';
const Box = (props) => {
    return (
        <div className={classes.box}>
            <div className={classes['box__title-wrap']}>
                <Link to={props.linkTo}><h3 className={classes['box__title']}>{props.title}</h3></Link>
            </div>
            <div className={classes['box__bgImg']} style={{backgroundImage: `url(${props.image})`}}></div>
        </div>
    )
}

export default Box;