import { Link } from 'react-router-dom';

const ImageItem = (props) => {
    return (
        <div>
            <Link to={props.linkTo}><img {...props.img} alt={props.img.alt}/></Link>
        </div>
    );
}

export default ImageItem;